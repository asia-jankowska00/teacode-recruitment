# Teacode Recruitment

> Teacode Recruitment Task

## How to run locally

1. Clone this repo.
2. Cd into the root directory and run `npm install`.
3. Run `npm start`and go to [http://localhost:3000](http://localhost:3000) to view it in the browser.

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
