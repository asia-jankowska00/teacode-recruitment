import React from "react";
// import "./tailwind.css";
import Container from "./components/Container";

function App() {
  return (
    <div className="bg-gray-300 min-h-screen flex justify-center py-10">
      <Container></Container>
    </div>
  );
}

export default App;
