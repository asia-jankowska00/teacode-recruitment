import React from "react";
import ReactDOM from "react-dom";
import "./tailwind.css";
import "./main.css";

import App from "./App";
import * as serviceWorker from "./serviceWorker";

import { ContactsProvider } from "./contexts/ContactsContext";

ReactDOM.render(
  <React.StrictMode>
    <ContactsProvider>
      <App />
    </ContactsProvider>
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
