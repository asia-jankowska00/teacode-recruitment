import React from "react";

const Header = (props) => {
  return (
    <h1 className="bg-teal-600 rounded-lg text-2xl text-gray-100 p-4 text-center">
      {props.title}
    </h1>
  );
};

export default Header;
