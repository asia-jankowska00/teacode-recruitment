import React, { useState } from "react";

const Searchbox = (props) => {
  const search = props.search;
  const setSearch = props.setSearch;

  return (
    <div className="w-full flex items-center p-2 text-gray-500">
      <label htmlFor="search">
        <span className="material-icons text-3xl">search</span>
      </label>
      <input
        id="search"
        type="text"
        placeholder="Search"
        onChange={(e) => {
          setSearch(e.target.value);
        }}
        className="p-2 flex-1 bg-gray-100"
      />
    </div>
  );
};

export default Searchbox;
