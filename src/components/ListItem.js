import React from "react";

const ListItem = (props) => {
  const selected = props.selected;
  const setSelected = props.setSelected;

  let data = {};
  props.data ? (data = props.data) : (data = {});

  return (
    <div
      className="w-full flex items-center justify-between p-4 cursor-pointer"
      onClick={() => {
        selected.includes(data.id)
          ? setSelected(selected.filter((id) => id !== data.id))
          : setSelected([...selected, data.id]);
      }}
    >
      {data.avatar ? (
        <img
          src={data.avatar}
          alt="Avatar"
          className="border border-gray-400 rounded-full"
        />
      ) : (
        <div className="border border-gray-400 bg-gray-400 rounded-full w-12 h-12"></div>
      )}

      <p className="flex-1 ml-4">
        {data.first_name} {data.last_name}
      </p>
      <input
        type="checkbox"
        checked={selected.includes(data.id) ? "checked" : ""}
        onChange={() => {
          selected.includes(data.id)
            ? setSelected(selected.filter((id) => id !== data.id))
            : setSelected([...selected, data.id]);
        }}
        className="h-6 w-6 bg-teal-600"
      />
    </div>
  );
};

export default ListItem;
