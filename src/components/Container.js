import React, { useContext, useEffect, useState } from "react";
import Header from "./Header";
import Searchbox from "./Searchbox";
import ListItem from "./ListItem";

import { ContactsContext } from "../contexts/ContactsContext";

const Container = () => {
  const [contacts, setContacts] = useState([]);
  const [viewContacts, setViewContacts] = useState([]);
  const [selected, setSelected] = useState([]);
  const [search, setSearch] = useState();

  const contactsContext = useContext(ContactsContext);

  useEffect(() => {
    console.log("Selected:", selected);
  }, [selected]);

  useEffect(() => {
    setContacts(
      contactsContext
        .slice(0, 100)
        .sort((a, b) => a.last_name.localeCompare(b.last_name))
    );
    setViewContacts(
      contactsContext
        .slice(0, 100)
        .sort((a, b) => a.last_name.localeCompare(b.last_name))
    );
  }, [contactsContext]);

  useEffect(() => {
    const filteredContacts = contacts.filter((contact) => {
      const fullName = `${contact.first_name} ${contact.last_name}`;
      if (
        fullName
          .toLowerCase()
          // lazy fix for silencing the linter when special characters are entered into the input
          .search(
            search.replaceAll("\\", "").replaceAll("[", "").toLowerCase()
          ) !== -1
      ) {
        return contact;
      }
    });

    setViewContacts(filteredContacts);
  }, [search]);

  return (
    <div className="w-3/4 md:w-1/2 lg:w-1/3 bg-gray-100 border border-gray-400 rounded-lg text-gray-700">
      <Header title="Contacts"></Header>
      <div className="p-4">
        <Searchbox search={search} setSearch={setSearch} />
        <div className=" divide-y divide-gray-400">
          {viewContacts.map((contact) => {
            return (
              <ListItem
                selected={selected}
                setSelected={setSelected}
                key={contact.id}
                data={contact}
              />
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default Container;
