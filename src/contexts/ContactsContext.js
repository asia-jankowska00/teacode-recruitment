import React, { createContext, useEffect, useState } from "react";
import axios from "axios";

export const ContactsContext = createContext();

export const ContactsProvider = (props) => {
  const [contacts, setContacts] = useState([]);

  useEffect(() => {
    axios
      .get(
        "https://cors-anywhere.herokuapp.com/https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json"
      )
      .then((res) => setContacts(res.data))
      .catch((err) => console.log(err));
  }, []);

  return (
    <ContactsContext.Provider value={contacts}>
      {props.children}
    </ContactsContext.Provider>
  );
};
